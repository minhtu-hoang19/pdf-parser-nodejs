# pdf-parser-nodejs

This repo will have some code examples for parsing PDFs with NodeJS
streams, as per [this][1] blog post. As further posts go on in the
series, the code will be accessible here for you to use (and engage
with me here if you've got any ideas / suggestions).

[1]: todo:REFME
